# infra-database-frontend

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Build and launch docker image
```
./docker-build.sh
./docker-run.sh
```

### Access
http://localhost:8000

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
